
require('dotenv').config({
  //path: `.env.${process.env.NODE_ENV}`,
  path: `.env`
})

module.exports = {
  siteMetadata: {
      title: `valleykind`,
    siteUrl: `https://www.yourdomain.tld`
  },
  plugins: [
    "gatsby-plugin-image", 
    "gatsby-plugin-sharp", 
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    "gatsby-transformer-sharp", {
      resolve: 'gatsby-source-filesystem',  
      resolve: `gatsby-plugin-google-fonts`,
        options: {
          fonts: [
            `Josefin Sans`,
          ],
          display: 'swap'
      },
      resolve: 'gatsby-source-filesystem',
        options: {
          name: 'pages',
          path: `${__dirname}/src/pages`,
      },
      resolve: "gatsby-plugin-anchor-links",
        options: {
          offset: -100
        }
      ,
      resolve: 'gatsby-source-prismic',
        options: {
          repositoryName: "valleykind",
          linkResolver: require("./src/LinkResolver").linkResolver,
          schemas: {
            main_page: require('./custom_types/main_page.json'),
            navigation: require('./custom_types/navigation.json'),
            page: require('./custom_types/page.json'),
          },
        },
      __key: "images"
  }]
};