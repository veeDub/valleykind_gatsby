import React from 'react'
import { graphql } from 'gatsby'
import { SliceZone } from '@prismicio/react'
import { PageLayout } from '../components/PageLayout' 
import { components } from '../slices'

const PageTemplate = ({data}) => {
    if(!data) return null
    const body = data.prismicPage.data.body
    const title = data.prismicPage.data.page_title.richText[0].text
    return (
        <PageLayout>
            <div className = {"Page"}>
                <div className = {"Page--Title"} >
                    <p className = {"Page--TitleText"}>{title}</p>
                </div>
            </div>
            <SliceZone slices = { body } components = { components } />
        </PageLayout>
    )
}

export const query = graphql`
query PageQuery($id: String) {
    prismicPage(id: { eq: $id }) {
      data {
        body {
          ... on PrismicPageDataBodyBlurb {
            id
            primary {
              blurb {
                richText
              }
            }
            slice_label
            slice_type
          }
          ... on PrismicPageDataBodyFullSizePhoto {
            id
            primary {
              image_2000_1200 {
                gatsbyImageData(layout: FIXED, placeholder: BLURRED)
                url
              }
            }
          }
          ... on PrismicPageDataBodyImageblurbcolumn {
            id
            slice_label
            slice_type
            primary {
              blurbright {
                richText
              }
              image_left {
                gatsbyImageData(placeholder: BLURRED, layout: FIXED)
              }
            }
          }
        }
        page_title {
          richText
        }
      }
    }
  }
  
`

export default PageTemplate