import React, { useState } from "react"
import { graphql } from 'gatsby'
import { getImage } from "gatsby-plugin-image"
import { SliceZone } from "@prismicio/react"
import './main.scss'

import { VersionContext } from '../contexts/version-context'
import { Header } from "../components/Header"
import { PageLayout } from "../components/PageLayout"
import {SetPageVersionContext} from "../components/SetPageVersionContext"
import { components } from "../slices"

const IndexPage = ({data}) => {
  const [pageVersion, setPageVersion] = useState("none")
  const doc = data.prismicMainPage.data
  let title = doc.main_page_title.richText[0].text
  let tagline = doc.main_page_tagline.richText[0].text
  let image = getImage(doc.main_page_main_image)
  let bodyClass = "Body--Body__Invisible"
  if(pageVersion !== "none"){
    bodyClass = "Body--Body__Visible"
  }

  // offsetY is what gives us our parallax effect
  // has to be passed down via context as we can't pass props into sliceZone
  const [offsetY, setOffsetY] = useState(0);                  
  return (
      <VersionContext.Provider value = {[[pageVersion, setPageVersion], [offsetY, setOffsetY]]}>
          <div className = "Content">
            <PageLayout>
              <Header 
                title = {title}
                tagline = {tagline}
                image = {image}
              />
              <SetPageVersionContext />
              <div className = {bodyClass} id = {"link0"}>
                <SliceZone slices = { doc.body } components = {components}/>
              </div>
            </PageLayout> 
          </div>
      </VersionContext.Provider>
    )
}

export const query = graphql`
  query MainPageQuery {
    prismicMainPage {
      data {
        body {
          ... on PrismicMainPageDataBodyPagesection {
            id
            primary {
              devsectiontext {
                richText
              }
              devsectiontitle {
                richText
              }
              nondevsectiontext {
                richText
              }
              nondevsectiontitle {
                richText
              }
              sectionimage {
                fluid(maxWidth: 2000, placeholderImgixParams: {blur: 10}) {
                  src
                  srcSet
                  srcWebp
                  srcSetWebp
                }
              }
              position
            }
            slice_label
            slice_type
          }
        }
        main_page_main_image {
          gatsbyImageData(placeholder: BLURRED, srcSetMaxWidth: 2000, layout: FULL_WIDTH)
          fluid(placeholderImgixParams: {blur: 10}) {
            src
            srcSet
            srcSetWebp
            srcWebp
          }
        }
        main_page_tagline {
          richText
        }
        main_page_title {
          richText
        }
      }
    }
  }

`
// export const query = graphql`
//   query HeaderQuery {
//     prismicMainPage {
//       data {
//         body {
//           ... on PrismicMainPageDataBodyPagesection {
//             id
//             slice_label
//             slice_type
//             primary {
//               sectionimage {
//                 fluid(maxWidth: 1920) {
//                   src
//                   srcSet
//                   srcWebp
//                 }
//               }
//               sectiontext {
//                 richText
//               }
//               sectiontitle {
//                 richText
//               }
//               css_id
//             }
//           }
//         }
//         main_page_main_image {
//           gatsbyImageData(layout: FULL_WIDTH, placeholder: BLURRED)
//         }
//         main_page_tagline {
//           richText
//         }
//         main_page_title {
//           richText
//         }
//       }
//     }
//   }
// `
export default IndexPage