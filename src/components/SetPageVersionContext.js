import React, { useContext } from 'react'
import { VersionContext } from '../contexts/version-context'

export const SetPageVersionContext = (props) => {
    const [pageVersion, setPageVersion] = useContext(VersionContext)[0]
    let containerClass = "Version--Container__Visible"
    let boxClass =  "Version--Box__Visible"
    if(pageVersion !== "none"){
       containerClass = "Version--Container__Invisible"
       boxClass = "Version--Box__Invisible"
    }
    return(
        <div className = {containerClass}>
            <div className = {boxClass}>
                <div className = {"Version--Title"}>
                    <p>Before we begin, who are you?</p>
                    <div className = {"Version--Row"}>
                        <p className = {"Version--Option"} onClick = {() => setPageVersion("dev")}>Someone looking to hire a developer</p>
                    </div>
                    <div className = {"Version--Row"}>
                        <p className = {"Version--Option"} onClick = {() => setPageVersion("nonDev")} >Someone looking for a site</p>
                    </div>
                </div>
            </div>
        </div>
    )
}