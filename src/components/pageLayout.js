import * as React from 'react'
import {MainMenu} from './Menu'
import Footer from './Footer'

export const PageLayout = ({ children }) => {
    return (
        <div>
            <nav>
                <MainMenu />
            </nav>
            <main>
                {children}
            </main>
            <footer>
                <Footer />
            </footer>
        </div>
    )
}