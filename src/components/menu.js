import React, { useContext } from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import { AnchorLink } from "gatsby-plugin-anchor-links";

import { TitleBanner } from './TitleBanner'
import { VersionContext } from '../contexts/version-context'
import '../pages/main.scss'
export const MainMenu = () => {
    let [pageVersion, setPageVersion] = useContext(VersionContext)[0]
    let menuClass = "Menu__Invisible"
    if(pageVersion !== "none"){
        menuClass = "Menu__Visible"
    }
    const query = useStaticQuery(gquery)
    const menuData = query.prismicNavigation.data.top_menu
    
    
    return(
        <div className = {menuClass}>
            <div  className = {"Menu--MenuDiv"}>
                <TitleBanner />
                {
                    menuData.map((menuItem, index) => {
                            let heading = menuItem.menu_item_heading.text
                            let linkUID = menuItem.link.uid
                            return (
                                <div  key={index}>
                                    <li key={index} className = {"Menu--MenuItemDiv"}>
                                        <div  className = {"Menu--MenuItem"} >
                                            <AnchorLink to = {'/#link' + (index + 1)} title = {heading}>{heading}</AnchorLink>
                                        </div>
                                    </li>
                                </div>
                            )
                    })
                }
            </div>
            
        </div>
        
    )
}

const gquery = graphql`
    query MenuQuery {
        prismicNavigation {
            data {
                menu_name {
                    text
                }
                top_menu {
                    link {
                        uid
                        slug
                    }
                    menu_item_heading {
                        text
                    }
                }
            }
        }
    }

`