import React from 'react'
import '../pages/main.scss'

const Footer = () => {
    return (
        <div className = "Footer">
            <p className = "Footer--Email">info@valleykind.com</p>
        </div>
    )
}

export default Footer