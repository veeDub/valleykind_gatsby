import React from 'react'
import { AnchorLink } from "gatsby-plugin-anchor-links";
 
export const TitleBanner = () => {
    return(
        <div className = {"Menu--TitleBanner"}>
            <AnchorLink className = {"Menu--TitleItem"} to = {'/#link0'} title = "valleyKind">valleyKind</AnchorLink>
        </div>
    )
}