import '../pages/main.scss'
import React, {useContext} from 'react'
import { GatsbyImage } from 'gatsby-plugin-image'
import { VersionContext } from '../contexts/version-context'

export const Header = ({
    title,
    tagline,
    image
}) => {
    let pageVersion = useContext(VersionContext)[0][0]
    let titleClass = pageVersion == "none" ? "Header--TitleText__Visible" : "Header--TitleText__Invisible"
    let descClass = pageVersion == "none" ? "Header--DescriptionText__Visible" : "Header--DescriptionText__Invisible"

    return(
        <div className = "Header">
            <GatsbyImage image = {image} alt = {"main image"} src = {image} className = "Header--Section1Image"/>
            <div className="Header--TitleBackground">
                <div className = "Header--TitleDiv"><p className = {titleClass}>{title}</p></div>
                <div className = "Header--DescriptionDiv"><p className = {descClass}>{tagline}</p></div>
            </div>
        </div>
                
    )
}