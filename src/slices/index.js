import {MainPageSection} from './MainPageSection'
import { Page2Column } from './Page2Column'
import { PageBlurb } from './PageBlurb'
import { PageFullSizePhoto } from './PageFullSizePhoto'

export const components = {
    pagesection: MainPageSection,
    blurb: PageBlurb,
    imageblurb: Page2Column,
    imagefullwidth: PageFullSizePhoto,
    imageblurbcolumn: Page2Column
}