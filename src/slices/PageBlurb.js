import React from 'react'
import { graphql } from 'gatsby'
import { PrismicRichText } from '@prismicio/react'

export const PageBlurb = ({ slice }) => {
    console.log("PAGE_BLURB")
    console.log("PAGE_BLURB: ", slice)
    let blurb = slice.primary.blurb.richText
    return(
        <div className = {"Blurb"}>
            <div className = {"Blurb--Text"}>
                <PrismicRichText field ={blurb}></PrismicRichText>
            </div>
        </div>
    )
}

export const query = graphql`
    fragment PageDataBodyBlurb on PrismicPageDataBodyBlurb{
        primary{
            blurb{
                richText
            }
        }
    }
`