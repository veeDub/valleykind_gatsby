import React from 'react'
import { graphql} from 'gatsby'
import { PrismicRichText } from '@prismicio/react'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'

export const Page2Column = ({slice}) => {
    let blurb = slice.primary.blurbright.richText
    let image = getImage(slice.primary.image_left)
    return(
        <div className = "BlurbImageSlice">
            <div className = {"BlurbImageSlice--Image"}>
                <GatsbyImage image = {image} src = {image} alt ={"Flowers"} />
            </div>
            <div className = {"BlurbImageSlice--Blurb"}>
                <PrismicRichText field ={blurb}></PrismicRichText>
            </div>
        </div>
        
        
    )
}

export const query = graphql`
    fragment PageDataBodyImageblurbcolumn on PrismicPageDataBodyImageblurbcolumn{
        primary {
            blurbright {
              richText
            }
            image_left {
              gatsbyImageData(placeholder: BLURRED, layout: FIXED)
            }
          }
    }
`