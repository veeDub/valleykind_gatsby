import React, {useEffect, useContext, useCallback} from 'react'
import { PrismicRichText } from '@prismicio/react'
import '../pages/main.scss'
import BackgroundImage from 'gatsby-background-image'
import { VersionContext } from '../contexts/version-context'

export const MainPageSection = ({ slice }) => {   
    const pos = slice.primary.position
    let pageVersion = useContext(VersionContext)[0][0]
    const setOffsetY = useContext(VersionContext)[1][1]
    let isImage = slice.primary.sectionimage.fluid
    const handleScroll = useCallback(() =>{ 
            setOffsetY(window.pageYOffset)
        }, [setOffsetY]
    );

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => window.removeEventListener("scroll", handleScroll);
    });
    
    let title
    let text 
    switch(pageVersion){
        case "dev":
            title = slice.primary.devsectiontitle.richText
            text = slice.primary.devsectiontext.richText
            break;
        case "nonDev":
            title = slice.primary.nondevsectiontitle.richText
            text = slice.primary.nondevsectiontext.richText
            break
        case "none":
            title = "none"
            text = "none"
            break
        default:
            title = "none"
            text = "none"
    }
    let image
    if(isImage){
        image = slice.primary.sectionimage.fluid
    }
    const ParallaxStyleImage = {
        width: "100%",
        height: "800px",
        backgroundPosition: "center center",
        backgroundSize: "cover",
        backgroundAttachment: "fixed"
    }
    const SectionTitle = {
        width: "100vw",
        position: "absolute",
        top: (pos * 500) + 30,
        display: "flex",
        alignContent: "center",
        justifyContent: "center"
    }
    
    const SectionText = {
        position: "absolute",
        top: (pos * 500) 
    }
    
    return(
        title !== "none"
        ?   
            isImage 
            ?
                <div id = {"link" + pos}>
                    <BackgroundImage
                        Tag="section"
                        style = {ParallaxStyleImage}
                        fluid = {image}
                        backgroundColor={`#333333`}
                    >
                    <div className = {"Body--SectionBanner"} >
                        <div className = {"Body--SectionTitle"}>
                            <PrismicRichText field = {title} />
                        </div>
                    </div>
                        
                    </BackgroundImage>

                    <div className = {SectionText} >
                        <div className = {"Body--SectionText"}>
                            <PrismicRichText field = {text} />
                        </div>
                    </div> 
                </div>
            :
                <div>
                    <div style = {SectionTitle}>
                        <div className = {"Body--SectionTitle"}>
                            <PrismicRichText field = {title} />
                        </div>
                    </div> 

                    <div className = {SectionText} >
                        <div className = {"Body--SectionText"}>
                            <PrismicRichText field = {text} />
                        </div>
                    </div> 
                </div>
        :    ""   
    )
}