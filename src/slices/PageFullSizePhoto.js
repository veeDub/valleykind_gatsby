import React from 'react'
import { graphql } from 'gatsby'

export const PageFullSizePhoto = ({slice}) => {
    console.log("slice: ", slice)
    return(
        <p>Placeholder photo</p>
    )
}


export const query = graphql`
    fragment PageDataBodyFullSizePhoto on PrismicPageDataBodyFullSizePhoto{
        primary {
            image_2000_1200 {
            gatsbyImageData
            }
        }
    }
`